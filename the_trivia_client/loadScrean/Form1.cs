﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


using System.Threading;


namespace loadScrean
{
    public partial class From1 : Form
    {
        // init the sound.
        System.Media.SoundPlayer the_grock_sound = new System.Media.SoundPlayer(loadScrean.Properties.Resources.the_grock_sound);

        public From1()
        {
            InitializeComponent();
            Thread getMassege = new Thread(magshimimLogo);
            getMassege.Start();
        }

        // טעינת סמל מגשימים למסך
        public void magshimimLogo()
        {
            the_grock_sound.Play(); // play the music of the grock.
            System.Threading.Thread.Sleep(3000);
            this.BackgroundImage = null;

            // קביעת מיקום ראשוני
            Point nextLocation = new Point(240, 110);
            
            // ."הקצאת החלק הראשון של תמונת הלוגו "מג
            System.Windows.Forms.PictureBox currPic = new PictureBox();
            currPic.Name = "mag"; // נתינת שם
            currPic.Image = global::loadScrean.Properties.Resources.mag; // ."בחירת "מג
            currPic.Location = nextLocation; // מיקום
            currPic.Size = new System.Drawing.Size(72, 52); // גודל
            currPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage; // איך תראה התמונה בהתאם לגודל הבלוק
            Invoke((MethodInvoker)delegate { this.Controls.Add(currPic); }); // הוספת התמונה
            nextLocation.X -= currPic.Width + 8; // המיקום הבא
            nextLocation.Y -= 5; // המיקום הבא


            System.Threading.Thread.Sleep(1000); // top the time for one second.


            // ."הקצאת החלק הראשון של תמונת הלוגו "שי
            System.Windows.Forms.PictureBox currPic2 = new PictureBox();
            currPic2.Name = "shi"; // נתינת שם
            currPic2.Image = global::loadScrean.Properties.Resources.shi; // ."בחירת "שי
            currPic2.Location = nextLocation; // מיקום
            currPic2.Size = new System.Drawing.Size(80, 58); // גודל
            currPic2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage; // איך תראה התמונה בהתאם לגודל הבלוק
            Invoke((MethodInvoker)delegate { this.Controls.Add(currPic2); }); // הוספת התמונה
            nextLocation.X -= currPic2.Width + 17; // המיקום הבא
            nextLocation.Y += 4; // המיקום הבא


            System.Threading.Thread.Sleep(1000); // top the time for one second.


            // ."הקצאת החלק הראשון של תמונת הלוגו "מים
            System.Windows.Forms.PictureBox currPic3 = new PictureBox();
            currPic3.Name = "mim"; // נתינת שם
            currPic3.Image = global::loadScrean.Properties.Resources.mim; // ."בחירת "מים
            currPic3.Location = nextLocation; // מיקום
            currPic3.Size = new System.Drawing.Size(97, 54); // גודל
            currPic3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage; // איך תראה התמונה בהתאם לגודל הבלוק
            Invoke((MethodInvoker)delegate { this.Controls.Add(currPic3); }); // הוספת התמונה



            System.Threading.Thread.Sleep(1500); // top the time for 1.5 seconds.
            callToHomeWindow();
        }

        // מנסה להציג את חלון מסך הבית
        public void callToHomeWindow()
        {
            Invoke((MethodInvoker)delegate { this.Hide(); });

            home newHomeWindow = new home();

            // if home is closed because the user don't have server,
            // and the user clicked "cancel" in messageBox, is exeption.
            try
            {
                newHomeWindow.ShowDialog();
            }
            catch (Exception) { }

            Invoke((MethodInvoker)delegate { this.Close(); });
        }
    }
}
