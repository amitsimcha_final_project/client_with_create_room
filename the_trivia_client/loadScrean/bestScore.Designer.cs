﻿namespace loadScrean
{
    partial class bestScore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.currnameLabel = new System.Windows.Forms.Label();
            this.winnersLabel = new System.Windows.Forms.Label();
            this.place1Label = new System.Windows.Forms.Label();
            this.place2Label = new System.Windows.Forms.Label();
            this.place3Label = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.podiumPictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.podiumPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Black;
            this.button1.Font = new System.Drawing.Font("Guttman-Aharoni", 10F, System.Drawing.FontStyle.Bold);
            this.button1.ForeColor = System.Drawing.Color.Gold;
            this.button1.Location = new System.Drawing.Point(628, 366);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(141, 35);
            this.button1.TabIndex = 7;
            this.button1.Text = "בחזרה למסך הבית";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // currnameLabel
            // 
            this.currnameLabel.AutoSize = true;
            this.currnameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.currnameLabel.Location = new System.Drawing.Point(12, 9);
            this.currnameLabel.Name = "currnameLabel";
            this.currnameLabel.Size = new System.Drawing.Size(0, 24);
            this.currnameLabel.TabIndex = 8;
            // 
            // winnersLabel
            // 
            this.winnersLabel.AutoSize = true;
            this.winnersLabel.Font = new System.Drawing.Font("Guttman Mantova-Decor", 32F);
            this.winnersLabel.Location = new System.Drawing.Point(35, 45);
            this.winnersLabel.Name = "winnersLabel";
            this.winnersLabel.Size = new System.Drawing.Size(700, 57);
            this.winnersLabel.TabIndex = 9;
            this.winnersLabel.Text = ":והזוכים בעבודות כפייה אצל גרגמל הם";
            // 
            // place1Label
            // 
            this.place1Label.AutoSize = true;
            this.place1Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.place1Label.Location = new System.Drawing.Point(312, 138);
            this.place1Label.Name = "place1Label";
            this.place1Label.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.place1Label.Size = new System.Drawing.Size(0, 20);
            this.place1Label.TabIndex = 10;
            // 
            // place2Label
            // 
            this.place2Label.AutoSize = true;
            this.place2Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.place2Label.Location = new System.Drawing.Point(92, 177);
            this.place2Label.Name = "place2Label";
            this.place2Label.Size = new System.Drawing.Size(0, 20);
            this.place2Label.TabIndex = 11;
            // 
            // place3Label
            // 
            this.place3Label.AutoSize = true;
            this.place3Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.place3Label.Location = new System.Drawing.Point(522, 198);
            this.place3Label.Name = "place3Label";
            this.place3Label.Size = new System.Drawing.Size(0, 20);
            this.place3Label.TabIndex = 12;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Black;
            this.button2.Font = new System.Drawing.Font("Guttman-Aharoni", 10F, System.Drawing.FontStyle.Bold);
            this.button2.ForeColor = System.Drawing.Color.Gold;
            this.button2.Location = new System.Drawing.Point(12, 366);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(141, 35);
            this.button2.TabIndex = 13;
            this.button2.Text = "?מי זה בדיוק גרגמל";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // podiumPictureBox
            // 
            this.podiumPictureBox.Image = global::loadScrean.Properties.Resources.podium;
            this.podiumPictureBox.Location = new System.Drawing.Point(12, 105);
            this.podiumPictureBox.Name = "podiumPictureBox";
            this.podiumPictureBox.Size = new System.Drawing.Size(757, 233);
            this.podiumPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.podiumPictureBox.TabIndex = 6;
            this.podiumPictureBox.TabStop = false;
            // 
            // bestScore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(781, 413);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.place3Label);
            this.Controls.Add(this.place2Label);
            this.Controls.Add(this.place1Label);
            this.Controls.Add(this.winnersLabel);
            this.Controls.Add(this.currnameLabel);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.podiumPictureBox);
            this.Name = "bestScore";
            this.Text = "bestScore";
            ((System.ComponentModel.ISupportInitialize)(this.podiumPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox podiumPictureBox;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label currnameLabel;
        private System.Windows.Forms.Label winnersLabel;
        private System.Windows.Forms.Label place1Label;
        private System.Windows.Forms.Label place2Label;
        private System.Windows.Forms.Label place3Label;
        private System.Windows.Forms.Button button2;
    }
}